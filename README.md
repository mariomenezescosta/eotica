# MongoDB Standalone EOTICA

The purpose of this repository is to deploy a simple service using mongodb in Kubernetes cluster.

## Getting Started

These instructions will get you a copy of the project up and running on your Kubernetes cluster for development and testing purposes.

### Prerequisites

1. Last stable version of MongoDB 4 Standalone 
2. User and password for authentication: `user` / `30t1c4`
3. Memory resource limit of 600MB 
4. Namespace: mongodb 
5. Creates a Loadbalancer service for 27017 port
6. `Git` and `kubectl` properly configured
7. mongodb namespace in cluster


### Deployment

To deploy mongodb standalone, first you need to create a secret file with MongoDB credentials:
```
kubectl -n mongodb create secret generic mongo-config-file --from-literal=mongo_user=user --from-literal=mongo_pass='30t1c4'
```

Then apply all kubernetes files:
```
git clone https://bitbucket.org/mariomenezescosta/eotica.git

cd eotica/

kubectl -n mongodb apply -f kubernetes-mondodb-standalone/
```
And you should see as output, something like:

```
deployment.apps/mongodb-standalone created
service/mongodb-standalone-service created
```

### Check pod state

```
kubectl -n mongodb get pods
``` 
And you should see as output, something like:
```
NAME                READY   STATUS    RESTARTS  AGE
mongodb-standalone   1/1    Running     1/1     20m
```
### Check service with LoadBalancer

```
kubectl -n mongodb get services
```
And you should see as output, something like:

```
NAME                        TYPE          CLUSTER-IP      EXTERNAL-IP  PORT(S)          AGE
mongodb-standalone-service  LoadBalancer  10.100.147.228  a13d0e14be   27017:30368/TCP  20m
```
### Check logs from pod

```
kubectl -n mongodb logs -f pods/<mongodb_pod_name>
```
And you should see as output, something like:
```
2019-10-15T02:31:38.899+0000 I  NETWORK  [listener] connection accepted from 192.168.46.255:25998 #113 (1 connection now open)
2019-10-15T02:31:38.899+0000 I  NETWORK  [listener] connection accepted from 192.168.47.98:24688 #114 (2 connections now open)
2019-10-15T02:31:38.899+0000 I  NETWORK  [listener] connection accepted from 192.168.43.242:49214 #115 (3 connections now open)
```
## Authors

* **Mario Costa** - *Initial work* - [Eotica](https://bitbucket.org/mariomenezescosta/eotica/src/master/)

## License

I don't know yet - sorry ;)
